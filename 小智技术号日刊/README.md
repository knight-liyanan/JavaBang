## 小智技术日刊更新分享

<!-- GFM-TOC -->
* [前言](#前言)
* [📖小智技术号日刊](#小智技术号日刊)
    * [🔥技术文章](#技术文章)
        * [MySQL 性能优化](#)
        * [Redis 进阶](#)
        * [Serverless](#Serverless)
    * [👀技术视频分享](#技术视频分享)
    * [💾资料分享](#资料分享)   

# 前言

## 小智技术号日刊

- [小智技术号日刊](https://t.1yb.co/4xqc)

### 技术文章
- **MySQL 性能优化**
    - [如何使用性能分析工具定位SQL执行慢的原因？](https://mp.weixin.qq.com/s/SXT_xDzN10vfKAYdNXZPaA)
    - [MySQL怎么查看 SQL 的具体执行成本？](https://mp.weixin.qq.com/s/aO3PxD6bZmtJJmUZVxD9zg)
    - [如何使用慢查询快速定位执行慢的 SQL?](https://mp.weixin.qq.com/s/0uf8vAhWu7Yk1sj37gzEjA)
    - [如果MySQL的 InnoDB 文件的损坏，该如何手动恢复？](https://mp.weixin.qq.com/s/i4aax-2V_gR1TeUt_jGZYg)
    - [数据库没有备份，没有使用Binlog的情况下，如何恢复数据？](https://mp.weixin.qq.com/s/At_40tDr3q_xCT72qLzaZQ)
    - [MySQL在grant语句之后要跟着flush privileges吗？](https://mp.weixin.qq.com/s/Bs57Nb12w_YdTiBWd34Rkw)
    - [MySQL为什么还有kill不掉的语句？](https://mp.weixin.qq.com/s/4VhCmyO2EpHJZrECJp_dxA)
    - [查询请求增加时，如何做主从分离？](https://mp.weixin.qq.com/s/C9HQ15t8rsjsAlLJBAj5xg)
    - [MySQL性能优化一：多种优化 方式介绍](https://mp.weixin.qq.com/s/3qfkfeZOcERR8AB4rzqO8w)
    - [MySQL性能优化(二)：优化数据库的设计](https://mp.weixin.qq.com/s/xafmfOOT3sEuDftdrVvZ8A)
    - [MySQL性能优化(三)：索引](https://mp.weixin.qq.com/s/OxBNSepOs1qFtAwguF8dfw)
    - [MySQL性能优化(四)：分表](https://mp.weixin.qq.com/s/LOY4Wo1ueGAij4CgMuxebw)
    - [MySQL性能优化(五)：分区](https://mp.weixin.qq.com/s/2TPMowGzhhOxZgs_TriMpg)
    - [MySQL性能优化(六)：其他优化](https://mp.weixin.qq.com/s/_NcY_7k02lIm-n5sOn9p2w)
    - [MySQL索引的原理，B+树、聚集索引和二级索引的结构分析](https://mp.weixin.qq.com/s/rWpooijYm3HeQZw3ESZq6w)
    - 更多内容请关注技术号～
  
- **Redis 进阶**
    - [宕机后，Redis如何实现快速恢复？](https://mp.weixin.qq.com/s/5GHiMu_KxmHW1sVsPsuXvw)
    - [Redis主从复制以及主从复制原理](https://mp.weixin.qq.com/s/0Y72MGcah8xcIJHU5f5NxA)
    - [用Redis构建缓存集群的最佳实践有哪些？](https://mp.weixin.qq.com/s/5eSAl8XpGgD-ZFs4ycdeXg)
    - [Redis数据增多了，是该加内存还是加实例？](https://mp.weixin.qq.com/s/oatPamGLW6ddRg2Ue5-vTA)
    - [用Redis构建缓存集群的最佳实践有哪些？](https://mp.weixin.qq.com/s/5eSAl8XpGgD-ZFs4ycdeXg)
    - [哨兵机制：主库挂了，如何不间断服务？](https://mp.weixin.qq.com/s/3eqOZItXMsj0rt3GNF6HOA)
    - 更多内容请关注技术号～

- **Serverless**
    - [阿里举集团之力趟坑Serverless,到底它能解决什么问题？](https://mp.weixin.qq.com/s/TSCqhHt34AQ0JjCM2KcRJw)

 
### 技术视频分享
- **短视频技术分享**
    - [两分钟解读Redis单机,主从,哨兵,集群架构](https://mp.weixin.qq.com/s/CSWafMGgEG9S4hycpD_lZw)
    - [分布式系统CAP原则](https://mp.weixin.qq.com/s/YxZyu1Q7VJT8J6PYXRr8uQ)
    - [Java并发编程之线程池实现原理](https://mp.weixin.qq.com/s/IGbQp2bKS2PfT_qYU1Pvkg)
    - [Redis 分布式锁的实现](https://mp.weixin.qq.com/s/1FIVEv7atvgoTe73-2PyRw)
    - [HashMap中hash算法有那些巧妙设计？](https://mp.weixin.qq.com/s/QpJUgJpMQsVj8ZcrQjfIeg)
    - [90秒解读Redis三种持久化方式](https://mp.weixin.qq.com/s/Qd35hL_fyCqCsRBEJa82zg)
    - 更多内容请关注技术号～
    
### 资料分享
   - [1000套微信小程序源码分享](https://mp.weixin.qq.com/s/9A4I23TLBzg8mCXRqvxp6A)
   - [福利｜Redis 全套面试题免费领取](https://mp.weixin.qq.com/s/DxNTX28e-cb4PaxGqa0qNQ)
   - [福利｜2020年BAT等一线互联网大厂春招Java面试题手册](https://mp.weixin.qq.com/s/dnI917h9sWY3Ve_schWTMA)
   - [福利｜Python 学习全套教程、手册限时免费领取](https://mp.weixin.qq.com/s/-sXA9ZQVYZNHXIX5TfXDHg)
   
   